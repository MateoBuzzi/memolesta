using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Tienda : MonoBehaviour
{
    public int INDRM;
    public Text monedas;

    private void Update()
    {
        monedas.text = INDRM.ToString();
    }
    public void tienda()
        {
            SceneManager.LoadScene("TiendaD");
        }

    public void Salir()
    {
        Application.Quit();
    }
    
    public void pase()
    {
        SceneManager.LoadScene("Pase");
    }

    public void Jugar()
    {
        SceneManager.LoadScene("Juego");
    }

    public void ruleta()
    {
        SceneManager.LoadScene("Ruleta");
    }

    public void opcion()
    {
        SceneManager.LoadScene("opciones");
    }  
    
    public void atras()
    {
        SceneManager.LoadScene("MeniInicial");
    }
    
    public void comprar()
    {
        INDRM += 1000;
        Debug.Log(INDRM);
        
    }
}
