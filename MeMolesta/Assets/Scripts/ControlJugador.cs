using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 9.0f;
    public Camera camaraEnPrimeraPersona;
    public GameObject proyectil;

    private int cont;


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cont = 0;

      

    }

 

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;


        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray rayo = camaraEnPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

            GameObject pro;
            pro = Instantiate(proyectil, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraEnPrimeraPersona.transform.forward * 40, ForceMode.Impulse);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }


    }
}

